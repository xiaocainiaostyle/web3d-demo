export default [
    {
        text: "快速上手",
        index: "1",
        items: [
            {
                text: "初始化",
                items: [
                    {
                        text: "场景初始化",
                        img: "./images/orillusion_init.png",
                        url: "orillusion_init.vue"
                    }
                ]
            }
        ]
    }
]
