export default [
    {
        text: "基础",
        index: "1",
        items: [
            {
                text: "初始化",
                items: [
                    {
                        text: "场景初始化",
                        img: "./images/正方体.png",
                        url: "three_init.vue"
                    }
                ]
            }
        ]
    },
    {
        text: "地图",
        index: "2",
        items: [
            {
                text: "可视化",
                items: [
                    {
                        text: "GeoJson行政区",
                        img: "",
                        url: "three_geojson.vue"
                    }
                ]
            }
        ]
    }
]
