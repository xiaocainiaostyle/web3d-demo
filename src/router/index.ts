import {createRouter, createWebHashHistory} from 'vue-router'
import ThreeIndex from '/src/components/three-examples/index.vue'
import OrillusionIndex from '/src/components/orillusion-examples/index.vue'
import Homepage from '/src/components/homepages/index.vue'
import Home from '/src/components/homepages/home.vue'
import ReplIndex from '/src/components/repl/repl.vue'

const routes: Array<any> = [
    {
        path: '/',
        name: '',
        component: Homepage,
        redirect: '/Homepage',
        children: [
            {
                path: '/Homepage',
                name: 'Homepage',
                component: Home
            },
            {
                path: '/Three',
                name: 'Three',
                component: ThreeIndex
            },
            {
                path: '/Orillusion',
                name: 'Orillusion',
                component: OrillusionIndex
            }
        ]
    },
    {
        path: '/three-examples/:examplespath',
        name: 'three-examples',
        component: () => import('@/components/three-examples/three-examplebox.vue')
    },
    {
        path: '/orillusion-examples/:examplespath',
        name: 'orillusion-examples',
        component: () => import('@/components/orillusion-examples/orillusion-examplebox.vue')
    },
    {
        path: '/Repl',
        name: 'Repl',
        component: ReplIndex
    }
]

const router = createRouter({
    history: createWebHashHistory(),
    routes
})

export default router