/*
 * @Author: Wang Jiyuan
 * @Date: 2023-05-04 17:36:06
 * @LastEditTime: 2023-06-19 10:58:38
 * @LastEditors: 
 * @Description: 
 */
import { createApp } from "vue";
import "./style.css";
import ElementPlus from "element-plus";
import "element-plus/dist/index.css";
import App from "./App.vue";
import * as ElementPlusIconsVue from "@element-plus/icons-vue";
import router from "./router"
import '@vue/repl/style.css';
import { TroisJSVuePlugin } from 'troisjs';

const app = createApp(App);

app.use(ElementPlus);
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component);
}
app.use(router)
app.use(TroisJSVuePlugin);
app.mount("#app");

