/*
 * @Author: Wang Jiyuan
 * @Date: 2023-05-04 17:35:04
 * @LastEditTime: 2023-06-21 10:58:51
 * @LastEditors: 
 * @Description: 
 */
import { defineConfig } from "vite";
import path from "path";
import vue from "@vitejs/plugin-vue";
import monacoEditorPlugin from "vite-plugin-monaco-editor"

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue(),monacoEditorPlugin()],
  resolve: {
    alias: {
        "@": path.resolve(__dirname, "src") ,
        // "vue": "vue/dist/vue.esm-bundler.js"
    },
    extensions: [".mjs", ".js", ".ts", ".jsx", ".tsx", ".json"],
  }, 
  base: "/web3d-demo/"
});
